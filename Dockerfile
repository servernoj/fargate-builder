FROM golang AS builder
# Create app directory
WORKDIR /
RUN git clone https://github.com/jpignata/fargate.git
RUN cd fargate; GOOS=linux GOARCH=amd64 go build -o /bin/fargate-linux-amd64
RUN cd fargate; GOOS=linux GOARCH=386 go build -o /bin/fargate-linux-386
# # -----------------------------------------------------------------
FROM amd64/docker:latest
WORKDIR /
# Install tools
RUN apk update
RUN apk upgrade
RUN apk add --no-cache bash
RUN apk add --no-cache curl jq python py-pip bash git
RUN pip --no-cache-dir install awscli
# Copy 
COPY --from=builder /bin/fargate-linux-amd64 /bin/
COPY --from=builder /bin/fargate-linux-386 /bin/
COPY bin/* /bin/
RUN ln -nsf /bin/fargate-linux-386 /bin/fargate